let counterFactoryFn = require("../counterFactory");

let result = counterFactoryFn();

console.log(result.decrement());
console.log(result.increment());
console.log(result.decrement());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());

