// Return an object that has two methods called `increment` and `decrement`.
// `increment` should increment a counter variable in closure scope and return it.
// `decrement` should decrement the counter variable and return it.

function counterFactory() {
  let counting = 0;
  return {
    increment: () => {
      return ++counting;

    },
    decrement: () => {
      return --counting;
    },
  };
}

module.exports = counterFactory;


